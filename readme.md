1. Выполнить команду в терминале : composer install
2. Создать файл в корне .env на основе .env.example
3. В файле .env установить переменные: APP_URL, DB_DATABASE, DB_USERNAME, DB_PASSWORD, MAIL_USERNAME, MAIL_PASSWORD, DASHBOARD_DOMAIN
4. Назначить папкам в корне права 777 (рекурсивно) : bootstrap, public, storage 
5. Выполнить команду в терминале : php artisan key:generate
6. Импортировать в бд файл в корне profstroy.sql
7. Создать ссылочную папку: public\storage storage\app\public

admin@admin.com - 123456
user@user.com - 123456