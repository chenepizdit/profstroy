<?php

namespace App\Http\Controllers;

use DB;
use Mail;
use Illuminate\Http\Request;
use Orchid\Press\Models\Menu;
use Orchid\Press\Models\Post;
use App\Http\Requests\MailRequest;

class AppController extends Controller
{

    private $menu;
    private $footer;
    private $setting;
    private $logo;

    public function __construct() {

        $this->menu = Menu::where('lang', app()->getLocale())
            ->where('parent',0)
            ->where('type', 'header')
            ->with('children')
            ->orderBy('sort')
            ->get();

        $this->footer = Post::where('slug', 'footer')->first();

        $this->setting = Post::where('slug', 'setting')->first();

        $imageMain = DB::table('attachments')
            ->join('attachmentable', 'attachmentable.attachment_id', '=', 'attachments.id')
            ->where('attachmentable.attachmentable_id', '=', $this->setting->id)
            ->first();

        $this->logo = $imageMain->path_image = '/storage/' . $imageMain->path . 
            '/' . $imageMain->name . '.' . $imageMain->extension;
        
    }

    public function index() {
        $main = Post::where('slug', 'main')->first();

        $imageMain = DB::table('attachments')
            ->join('attachmentable', 'attachmentable.attachment_id', '=', 'attachments.id')
            ->where('attachmentable.attachmentable_id', '=', $main->id)
            ->first(); 

        $image = $imageMain->path_image = '/storage/' . $imageMain->path . 
            '/' . $imageMain->name . '.' . $imageMain->extension;

        return $this->renderView('main', [
            'main' => $main,
            'imagePath' => $image,
        ]);
    }

    public function about() {
        $type = 'about';
        $content = Post::where('slug', $type)->first();

        return $this->renderView($type, [
            'crumbs' => $this->getCrumbs($type),
            'content' => $content,
        ]);
    }

    public function contact() {
        $type = 'contact';
        $content = Post::where('slug', $type)->first();

        return $this->renderView($type, [
            'crumbs' => $this->getCrumbs($type),
            'content' => $content,
        ]);
    }

    public function send(MailRequest $request) {
        $this->sendMail(
            $request['contact-name'], 
            $request['contact-email'],
            $request['textarea-message'],
            $request['contact-phone']
        );
    }

    private function sendMail($name, $email, $comment, $phone) {

        $toName = env('MAIL_NAME');
        $toEmail = env('MAIL_USERNAME');
        $fromEmail = env('MAIL_USERNAME');

        $data = [
            'name' => $name,
            'email' => $email,
            'comment' => $comment,
            'phone' => $phone,
        ]; 
        
        Mail::send('emails.mail', $data, function($message) use ($toName, $toEmail, $fromEmail) {
            $message->to($toEmail, $toName)
                    ->subject('ПРОФСТРОЙ - обратная связь');
            $message->from($fromEmail, 'Обратная связь');
        });

    }

    public function news() {
        $type = 'news';
        $content = Post::where('type', $type)->get();
       
        return $this->renderView($type, [
            'crumbs' => $this->getCrumbs($type),
            'content' => $content,
        ]);
    }

    public function one_news($slug) {
        $type = 'news';
        $content = Post::where('type', 'news')->where('slug', $slug)->first();
        
        if(empty($content)) {
            abort(404);
        }
        return $this->renderView('one_'.$type, [
            'crumbs' => $this->getCrumbs($type, $content->content['en']['name']),
            'content' => $content,
        ]);
    }

    public function project() {
        $type = 'project';
        $content = Post::where('type', $type)->get();

        return $this->renderView($type, [
            'crumbs' => $this->getCrumbs($type),
            'content' => $content,
        ]);
    }

    public function one_project($slug) {
        $type = 'project';
        $content = Post::where('type', $type)->where('slug', $slug)->first();

        if(empty($content)) {
            abort(404);
        }
        
        return $this->renderView('one_'.$type, [
            'crumbs' => $this->getCrumbs($type, $content->content['en']['name']),
            'content' => $content,
        ]);
    }

    public function getCrumbs($type, $title = null) {
        $item = !empty($title) ? (object)['label' => $title] : $title;
        $crumbs = [
            Menu::where('robot', 'first')->first(),
            Menu::where('title', $type)->first()->parent()->first(),
            Menu::where('title', $type)->first(),
            $item,
        ];
        return array_filter($crumbs);
    }

    private function renderView($template, $data) {
        $data['menu'] = $this->menu;
        $data['footer'] = $this->footer;
        $data['setting'] = $this->setting;
        $data['logo'] = $this->logo;
        return view($template, $data);
    }

}
