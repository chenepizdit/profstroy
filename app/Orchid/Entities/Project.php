<?php

declare(strict_types=1);

namespace App\Orchid\Entities;

use Orchid\Screen\TD;
use Orchid\Screen\Field;
use Orchid\Press\Entities\Many;
use Orchid\Press\Models\Category;
use Orchid\Screen\Fields\MapField;
use Orchid\Screen\Fields\UTMField;
use Orchid\Screen\Fields\CodeField;
use Orchid\Screen\Fields\TagsField;
use Orchid\Screen\Fields\InputField;
use Orchid\Screen\Fields\QuillField;
use Orchid\Screen\Fields\SelectField;
use Orchid\Screen\Fields\UploadField;
use Orchid\Screen\Fields\PictureField;
use Orchid\Screen\Fields\TinyMCEField;
use Illuminate\Database\Eloquent\Model;
use Orchid\Screen\Fields\CheckBoxField;
use Orchid\Screen\Fields\TextAreaField;
use Orchid\Screen\Fields\DateTimerField;
use Orchid\Screen\Fields\SimpleMDEField;
use Orchid\Press\Http\Filters\SearchFilter;
use Orchid\Press\Http\Filters\StatusFilter;
use Orchid\Press\Http\Filters\CreatedFilter;

class Project extends Many
{
    /**
     * @var string
     */
    public $name = 'Проекты';

    /**
     * @var string
     */
    public $description = 'Создание проектов';

    /**
     * @var string
     */
    public $slug = 'project';

    /**
     * Slug url /news/{name}.
     *
     * @var string
     */
    public $slugFields = 'name';

    /**
     * Menu group name.
     *
     * @var null
     */
    public $groupname = null;

    /**
     * @param \Illuminate\Database\Eloquent\Model $model
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function create(Model $model) : Model
    {
        return $model->load(['attachment', 'tags', 'taxonomies'])
            ->setAttribute('category', $model->taxonomies->map(function ($item) {
                return $item->id;
            })->toArray());
    }

    /**
     * @param \Illuminate\Database\Eloquent\Model $model
     */
    public function save(Model $model)
    {
        $model->save();

        $model->taxonomies()->sync(array_flatten(request(['category'])));
        $model->setTags(request('tags', []));
        $model->attachment()->syncWithoutDetaching(request('attachment', []));
    }

    /**
     * Rules Validation.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'id'             => 'sometimes|integer|unique:posts',
            'content.*.name' => 'required|string',
            'content.*.preview' => 'required|string',
            'content.*.body' => 'required|string',
        ];
    }

    /**
     * HTTP data filters.
     *
     * @return array
     */
    public function filters(): array
    {
        return [
            SearchFilter::class,
            StatusFilter::class,
            CreatedFilter::class,
        ];
    }

    /**
     * @return array
     * @throws \Throwable|\Orchid\Screen\Exceptions\TypeException
     */
    public function fields(): array
    {
        return [

            InputField::make('name')
                ->type('text')
                ->max(255)
                ->required()
                ->title('Название проекта')
                ->help('Название проекта'),

            TextAreaField::make('preview')
                ->max(255)
                ->maxlength(500)
                ->rows(5)
                ->title('Превью')
                ->help('Краткое описание проекта'),

            TinyMCEField::make('body')
                ->required()
                ->title('Содержание проекта')
                ->help('Содержание проекта')
                ->theme('modern'),

        ];
    }

    /**
     * @return array
     * @throws \Orchid\Screen\Exceptions\TypeException
     * @throws \Throwable
     */
    public function main(): array
    {
        return array_merge(parent::main(), []);
    }

    /**
     * @return array
     * @throws \Throwable
     */
    public function options(): array
    {
        return [];
    }

    /**
     * Grid View for post type.
     */
    public function grid(): array
    {
        return [
            TD::set('id', 'ID')
                ->align('center')
                ->width('100px')
                ->filter('numeric')
                ->sort()
                ->linkPost(),

            TD::set('name', 'Name')
                ->width('250px')
                ->locale()
                ->column('content.name')
                ->filter('text')
                ->sort()
                ->linkPost('name'),

            TD::set('status')
                ->sort(),

            TD::set('publish_at', 'Date of publication')
                ->filter('date')
                ->sort()
                ->align('right')
                ->setRender(function ($item) {
                    return optional($item->publish_at)->toDateString();
                }),

            TD::set('created_at', 'Date of creation')
                ->filter('date')
                ->align('right')
                ->sort()
                ->setRender(function ($item) {
                    return $item->created_at->toDateString();
                }),
        ];
    }
}
