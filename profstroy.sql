-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Мар 07 2019 г., 11:09
-- Версия сервера: 5.7.20
-- Версия PHP: 7.1.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `profstroy`
--

-- --------------------------------------------------------

--
-- Структура таблицы `announcements`
--

CREATE TABLE `announcements` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `attachmentable`
--

CREATE TABLE `attachmentable` (
  `id` int(10) UNSIGNED NOT NULL,
  `attachmentable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `attachmentable_id` int(10) UNSIGNED NOT NULL,
  `attachment_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `attachmentable`
--

INSERT INTO `attachmentable` (`id`, `attachmentable_type`, `attachmentable_id`, `attachment_id`) VALUES
(1, 'Orchid\\Press\\Models\\Page', 1, 1),
(10, 'Orchid\\Press\\Models\\Page', 9, 18);

-- --------------------------------------------------------

--
-- Структура таблицы `attachments`
--

CREATE TABLE `attachments` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `original_name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `mime` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `extension` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size` bigint(20) NOT NULL DEFAULT '0',
  `sort` int(11) NOT NULL DEFAULT '0',
  `path` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `alt` text COLLATE utf8mb4_unicode_ci,
  `hash` text COLLATE utf8mb4_unicode_ci,
  `disk` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'public',
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `group` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `attachments`
--

INSERT INTO `attachments` (`id`, `name`, `original_name`, `mime`, `extension`, `size`, `sort`, `path`, `description`, `alt`, `hash`, `disk`, `user_id`, `group`, `created_at`, `updated_at`) VALUES
(1, '46cd0ef062b632b035acd5458124904d751c5f35', 'bg_header.jpg', 'image/jpeg', 'jpg', 87734, 0, '2019/01/24\\', NULL, NULL, '6e872ec81b0dee499d5d956646024e921916b51a', 'public', 1, NULL, '2019-01-24 08:05:16', '2019-01-24 08:05:16'),
(2, 'e56811a4ade3121cba8706a70d07619a4c847e88', 'mac.jpg', 'image/jpeg', 'jpg', 37478, 0, '2019/01/24\\', NULL, NULL, 'ac10faa829f9f7aa672d6b1b3d0bd56a5151bad7', 'public', 1, NULL, '2019-01-24 08:39:46', '2019-01-24 08:39:46'),
(3, 'e56811a4ade3121cba8706a70d07619a4c847e88', 'mac.jpg', 'image/jpeg', 'jpg', 37478, 0, '2019/01/24\\', NULL, NULL, 'ac10faa829f9f7aa672d6b1b3d0bd56a5151bad7', 'public', 1, NULL, '2019-01-24 08:45:30', '2019-01-24 08:45:30'),
(4, 'e56811a4ade3121cba8706a70d07619a4c847e88', 'mac.jpg', 'image/jpeg', 'jpg', 37478, 0, '2019/01/24\\', NULL, NULL, 'ac10faa829f9f7aa672d6b1b3d0bd56a5151bad7', 'public', 1, NULL, '2019-01-24 08:46:39', '2019-01-24 08:46:39'),
(5, 'e56811a4ade3121cba8706a70d07619a4c847e88', 'mac.jpg', 'image/jpeg', 'jpg', 37478, 0, '2019/01/24\\', NULL, NULL, 'ac10faa829f9f7aa672d6b1b3d0bd56a5151bad7', 'public', 1, NULL, '2019-01-24 08:48:15', '2019-01-24 08:48:15'),
(6, 'bb0c36a37fea89f1c826097938260d71efe24a33', '2.jpg', 'image/jpeg', 'jpg', 11540, 0, '2019/01/24\\', NULL, NULL, '339bcad807ed5a904cd7703efab85ecb7b904dc9', 'public', 1, NULL, '2019-01-24 10:44:48', '2019-01-24 10:44:48'),
(7, '13158bc4cf71f71b56a9e629e19cf5867473e416', 'logo.png', 'image/png', 'png', 2074, 0, '2019/01/28\\', NULL, NULL, '2852978bd48a5b564434ecaf8b4f071aac14ef33', 'public', 1, NULL, '2019-01-28 04:22:22', '2019-01-28 04:22:22'),
(8, '13158bc4cf71f71b56a9e629e19cf5867473e416', 'logo.png', 'image/png', 'png', 2074, 0, '2019/01/28\\', NULL, NULL, '2852978bd48a5b564434ecaf8b4f071aac14ef33', 'public', 1, NULL, '2019-01-28 04:23:40', '2019-01-28 04:23:40'),
(9, '13158bc4cf71f71b56a9e629e19cf5867473e416', 'logo.png', 'image/png', 'png', 2074, 0, '2019/01/28\\', NULL, NULL, '2852978bd48a5b564434ecaf8b4f071aac14ef33', 'public', 1, NULL, '2019-01-28 04:24:02', '2019-01-28 04:24:02'),
(10, '13158bc4cf71f71b56a9e629e19cf5867473e416', 'logo.png', 'image/png', 'png', 2074, 0, '2019/01/28\\', NULL, NULL, '2852978bd48a5b564434ecaf8b4f071aac14ef33', 'public', 1, NULL, '2019-01-28 04:24:02', '2019-01-28 04:24:02'),
(11, 'e56811a4ade3121cba8706a70d07619a4c847e88', 'mac.jpg', 'image/jpeg', 'jpg', 37478, 0, '2019/01/24\\', NULL, NULL, 'ac10faa829f9f7aa672d6b1b3d0bd56a5151bad7', 'public', 1, NULL, '2019-01-28 04:27:20', '2019-01-28 04:27:20'),
(12, '598b1accbe54c542e2b3395b2c90e32945d99661', 'gt_favicon.png', 'image/png', 'png', 370, 0, '2019/01/28\\', NULL, NULL, '453e8b6e3e1f28e0c12f1ea181d6f8693168d13b', 'public', 1, NULL, '2019-01-28 04:27:32', '2019-01-28 04:27:32'),
(13, '13158bc4cf71f71b56a9e629e19cf5867473e416', 'logo.png', 'image/png', 'png', 2074, 0, '2019/01/28\\', NULL, NULL, '2852978bd48a5b564434ecaf8b4f071aac14ef33', 'public', 1, NULL, '2019-01-28 04:27:41', '2019-01-28 04:27:41'),
(14, 'bb0c36a37fea89f1c826097938260d71efe24a33', '2.jpg', 'image/jpeg', 'jpg', 11540, 0, '2019/01/24\\', NULL, NULL, '339bcad807ed5a904cd7703efab85ecb7b904dc9', 'public', 1, NULL, '2019-01-28 04:28:00', '2019-01-28 04:28:00'),
(15, 'ed36ff3b93994f1b32bb03a6ab74cc62cb3f2beb', '1.jpg', 'image/jpeg', 'jpg', 17652, 0, '2019/01/28\\', NULL, NULL, 'baf2492616d367f303923134bcbb69154ea80e60', 'public', 1, NULL, '2019-01-28 04:31:04', '2019-01-28 04:31:04'),
(16, '13158bc4cf71f71b56a9e629e19cf5867473e416', 'logo.png', 'image/png', 'png', 2074, 0, '2019/01/28\\', NULL, NULL, '2852978bd48a5b564434ecaf8b4f071aac14ef33', 'public', 1, NULL, '2019-01-28 04:31:12', '2019-01-28 04:31:12'),
(17, 'e56811a4ade3121cba8706a70d07619a4c847e88', 'mac.jpg', 'image/jpeg', 'jpg', 37478, 0, '2019/01/24\\', NULL, NULL, 'ac10faa829f9f7aa672d6b1b3d0bd56a5151bad7', 'public', 1, NULL, '2019-01-28 04:35:37', '2019-01-28 04:35:37'),
(18, '13158bc4cf71f71b56a9e629e19cf5867473e416', 'logo.png', 'image/png', 'png', 2074, 0, '2019/01/28\\', NULL, NULL, '2852978bd48a5b564434ecaf8b4f071aac14ef33', 'public', 1, NULL, '2019-01-28 04:35:47', '2019-01-28 04:35:47');

-- --------------------------------------------------------

--
-- Структура таблицы `comments`
--

CREATE TABLE `comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `post_id` int(10) UNSIGNED DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `approved` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `menu`
--

CREATE TABLE `menu` (
  `id` int(10) UNSIGNED NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `robot` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `style` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `target` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `auth` tinyint(1) NOT NULL DEFAULT '0',
  `lang` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` int(11) NOT NULL DEFAULT '0',
  `sort` int(11) NOT NULL DEFAULT '0',
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `menu`
--

INSERT INTO `menu` (`id`, `label`, `title`, `slug`, `robot`, `style`, `target`, `auth`, `lang`, `parent`, `sort`, `type`, `created_at`, `updated_at`) VALUES
(1, 'Главная', 'main', '/', 'first', NULL, '_self', 0, 'en', 0, 0, 'header', '2019-01-24 07:02:25', '2019-01-25 09:34:40'),
(2, 'Новости', 'news', '/news', NULL, NULL, '_self', 0, 'en', 0, 1, 'header', '2019-01-24 07:02:55', '2019-01-24 07:35:19'),
(3, 'Проекты', 'project', '/project', NULL, NULL, '_self', 0, 'en', 0, 4, 'header', '2019-01-24 07:03:46', '2019-01-28 03:53:00'),
(4, 'О нас', 'about', '/about', NULL, NULL, '_self', 0, 'en', 0, 5, 'header', '2019-01-24 07:04:35', '2019-01-28 03:56:35'),
(5, 'Контакты', 'contact', '/contact', NULL, NULL, '_self', 0, 'en', 0, 6, 'header', '2019-01-24 07:05:01', '2019-01-24 07:35:50'),
(6, 'Тест', 'test1', '/test1', NULL, NULL, '_self', 0, 'en', 2, 2, 'header', '2019-01-24 07:05:23', '2019-01-25 09:23:28'),
(7, 'Тест 2', 'test2', '/test2', NULL, NULL, '_self', 0, 'en', 2, 3, 'header', '2019-01-24 07:05:47', '2019-01-25 09:23:35');

-- --------------------------------------------------------

--
-- Структура таблицы `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2015_04_12_000000_create_orchid_users_table', 1),
(4, '2015_04_15_102754_create_orchid_tags_table', 1),
(5, '2015_04_15_105754_create_orchid_menu_table', 1),
(6, '2015_10_19_214424_create_orchid_roles_table', 1),
(7, '2015_10_19_214425_create_orchid_role_users_table', 1),
(8, '2015_12_02_181214_create_table_settings', 1),
(9, '2016_02_09_194940_create_orchid_post_table', 1),
(10, '2016_08_07_125128_create_orchid_attachmentstable_table', 1),
(11, '2016_12_06_070031_create_orchid_terms_table', 1),
(12, '2016_12_06_070036_create_orchid_taxonomy_table', 1),
(13, '2016_12_06_070037_create_orchid_comments_table', 1),
(14, '2016_12_06_070037_create_orchid_relationships_table', 1),
(15, '2017_09_17_125801_create_notifications_table', 1),
(16, '2018_09_16_190756_create_orchid_announcements_table', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `notifications`
--

CREATE TABLE `notifications` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_id` bigint(20) UNSIGNED NOT NULL,
  `data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `read_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `notifications`
--

INSERT INTO `notifications` (`id`, `type`, `notifiable_type`, `notifiable_id`, `data`, `read_at`, `created_at`, `updated_at`) VALUES
('78fd089f-20ff-4a47-9f75-334716da7448', 'Orchid\\Platform\\Notifications\\DashboardNotification', 'Orchid\\Platform\\Models\\User', 1, '{\"title\":\"Welcome admin\",\"message\":\"You can find the latest news of the project on the website\",\"action\":\"https:\\/\\/orchid.software\\/\",\"type\":\"text-info\",\"time\":{\"date\":\"2019-01-24 10:39:55.874793\",\"timezone_type\":3,\"timezone\":\"UTC\"}}', NULL, '2019-01-24 06:39:55', '2019-01-24 06:39:55');

-- --------------------------------------------------------

--
-- Структура таблицы `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` json DEFAULT NULL,
  `options` json DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `publish_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `posts`
--

INSERT INTO `posts` (`id`, `user_id`, `type`, `status`, `content`, `options`, `slug`, `publish_at`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'page', 'publish', '{\"en\": {\"body\": \"<p>The difference between involvement and commitment is like an eggs-and-ham breakfast: the chicken was involved; the pig was committed.</p>\", \"name\": \"Awesome, customizable, free\", \"heading\": \"The best place to tell people why they are here\", \"description\": \"PROGRESSUS: free business bootstrap template by\"}}', '{\"locale\": {\"en\": \"true\"}}', 'main', '2019-01-24 08:05:19', '2019-01-24 08:05:19', '2019-01-24 08:05:19', NULL),
(2, 1, 'page', 'publish', '{\"en\": {\"body\": \"<h3>Lorem ipsum</h3>\\r\\n<p><img class=\\\"img-rounded pull-right\\\" src=\\\"http://profstroy/storage/2019/01/24\\\\e56811a4ade3121cba8706a70d07619a4c847e88.jpg\\\" alt=\\\"\\\" width=\\\"300\\\" height=\\\"200\\\" />Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eveniet, consequuntur eius repellendus eos aliquid molestiae ea laborum ex quibusdam laudantium voluptates placeat consectetur quam aliquam beatae soluta accusantium iusto nihil nesciunt unde veniam magnam repudiandae sapiente.</p>\\r\\n<h3>Necessitatibus</h3>\\r\\n<p>Velit, odit, eius, libero unde impedit quaerat dolorem assumenda alias consequuntur optio quae maiores ratione tempore sit aliquid architecto eligendi pariatur ab soluta doloremque dicta aspernatur labore quibusdam dolore corrupti quod inventore. Maiores, repellat, consequuntur eius repellendus eos aliquid molestiae ea laborum ex quibusdam laudantium voluptates placeat consectetur quam aliquam!</p>\", \"name\": \"About us\"}}', '{\"locale\": {\"en\": \"true\"}}', 'about', '2019-01-24 08:52:14', '2019-01-24 08:39:17', '2019-01-24 08:52:15', NULL),
(3, 1, 'page', 'publish', '{\"en\": {\"body\": \"<p>We&rsquo;d love to hear from you. Interested in working together? Fill out the form below with some info about your project and I will get back to you as soon as I can. Please allow a couple days for me to respond.</p>\", \"name\": \"Contact us\", \"phone\": \"(713) 791-1414\", \"address\": \"2002 Holcombe Boulevard, Houston, TX 77030, USA\"}}', '{\"locale\": {\"en\": \"true\"}}', 'contact', '2019-01-24 09:25:22', '2019-01-24 09:25:22', '2019-01-24 09:25:22', NULL),
(4, 1, 'news', 'publish', '{\"en\": {\"body\": \"<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium, itaque, cumque, maxime obcaecati reprehenderit ea dignissimos amet voluptatem id excepturi facilis totam veritatis maiores eveniet neque explicabo temporibus quisquam in ex ab fugiat ipsa tempore sunt corporis nostrum quam illum!</p>\\r\\n<p>Consectetur cupiditate labore repudiandae beatae nisi fugiat facilis natus illum vitae doloremque. In, perspiciatis, natus, impedit voluptas itaque odio repudiandae placeat nisi totam repellendus earum dolores mollitia tempore quasi beatae alias cum dicta maxime laborum corporis harum porro magnam laboriosam.</p>\\r\\n<p>Aut, eaque, minus atque alias odio mollitia cum nisi ipsa nulla natus quae minima similique ipsam aspernatur molestias animi in deleniti nam. Tempora, labore, modi eum perspiciatis doloremque sequi nam illo corporis iusto maiores nisi recusandae repellat animi reiciendis accusamus.</p>\\r\\n<h2>A, quibusdam, nobis, eveniet consequatur</h2>\\r\\n<p>A, quibusdam, nobis, eveniet consequatur alias doloremque officia blanditiis fuga et numquam labore reiciendis voluptas quis repellat quos sunt non dolore consectetur at sit nam tenetur dolorem? Harum, quas, sit perspiciatis esse odit temporibus aperiam nulla aspernatur sequi fugiat tempore?</p>\\r\\n<p>Ad velit consequuntur quo qui odit quam sapiente repudiandae et ea pariatur? Ex sapiente beatae nobis consectetur ea. Deleniti, beatae, magnam, dolorum, fuga nostrum quas laboriosam sapiente temporibus enim voluptates ullam impedit atque quae provident quos mollitia aperiam perferendis amet.</p>\\r\\n<blockquote>Numquam, ut iure quia facere totam quas odit illo incidunt. Voluptatem, nostrum, ex, quasi incidunt similique cum maxime expedita unde labore inventore excepturi veniam corporis sequi facere ullam voluptates amet illum quam fuga voluptatibus ipsum atque sunt eos. Ut, necessitatibus.</blockquote>\\r\\n<p>Odit, laudantium, dolores, natus distinctio labore voluptates in inventore quasi qui nobis quis adipisci fugit id! Aliquam alias ea modi. Porro, odio, sed veniam hic numquam qui ad molestiae sint placeat expedita? Perferendis, enim qui numquam sequi obcaecati molestiae fugiat!</p>\\r\\n<p>Aperiam, odit, quasi, voluptate fugiat quisquam velit magni provident corporis animi facilis illo eveniet eum obcaecati adipisci blanditiis quas labore doloribus eos veniam repudiandae suscipit tempora ad harum odio eius distinctio hic deleniti. Sunt fuga ad perspiciatis repellat deleniti omnis!</p>\\r\\n<h3>Numquam, ut iure quia facere totam quas odit illo incidunt</h3>\\r\\n<p>Est, maiores, fuga sed nemo qui veritatis ducimus placeat odit quisquam dolorum. Rem, sunt, praesentium veniam maiores quia molestias eos fugit eaque ducimus veritatis provident assumenda. Quia, fuga, voluptates voluptatibus quis enim nam asperiores aliquam dignissimos ullam recusandae debitis iste.</p>\\r\\n<p>Dignissimos, beatae, praesentium illum eos autem perspiciatis? Minus, non, tempore, illo, mollitia exercitationem tempora quas harum odio dolores delectus quidem laudantium adipisci ducimus ullam placeat eaque minima quae iure itaque corporis magni nesciunt eius sed dolor doloremque id quasi nisi.</p>\", \"name\": \"Lorem ipsum dolor sit amet, consectetur.\", \"preview\": \"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium, itaque, cumque, maxime obcaecati reprehenderit ea dignissimos amet voluptatem id excepturi facilis totam veritatis maiores eveniet neque explicabo temporibus quisquam in ex ab fugiat ipsa tempore sunt corporis nostrum quam illum!\\r\\n\\r\\nConsectetur cupiditate labore repudiandae beatae nisi fugiat facilis natus illum vitae doloremque. In, perspiciatis, natus, impedit voluptas itaque odio repudiandae placeat nisi totam repellendus ear\"}}', '{\"locale\": {\"en\": \"true\"}}', 'lorem-ipsum-dolor-sit-amet-consectetur', '2019-01-24 10:40:15', '2019-01-24 10:40:15', '2019-01-24 10:43:40', NULL),
(5, 1, 'news', 'publish', '{\"en\": {\"body\": \"<h3>Doloribus, illo ipsum</h3>\\r\\n<p>Velit, odit, eius, libero unde impedit quaerat dolorem assumenda alias consequuntur optio quae maiores ratione tempore sit aliquid architecto eligendi pariatur ab soluta doloremque dicta aspernatur labore quibusdam dolore corrupti quod inventore. Maiores, repellat, consequuntur eius repellendus eos aliquid molestiae ea laborum ex quibusdam laudantium voluptates placeat consectetur quam aliquam!</p>\\r\\n<p>Eum, quasi, est, vitae, ipsam nobis consectetur ea aspernatur ad eos voluptatibus fugiat nisi perferendis impedit. Quam, nulla, excepturi, voluptate minus illo tenetur sint ab in culpa cumque impedit quibusdam. Saepe, molestias quia voluptatem natus velit fugiat omnis rem eos sapiente quasi quaerat aspernatur quisquam deleniti accusantium laboriosam odio id?<img src=\\\"http://profstroy/storage/2019/01/24\\\\bb0c36a37fea89f1c826097938260d71efe24a33.jpg\\\" alt=\\\"\\\" width=\\\"300\\\" height=\\\"195\\\" /></p>\", \"name\": \"Lorem ipsum\", \"preview\": \"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eveniet, consequuntur eius repellendus eos aliquid molestiae ea laborum ex quibusdam laudantium voluptates placeat consectetur quam aliquam beatae soluta accusantium iusto nihil nesciunt unde veniam magnam repudiandae sapiente.\\r\\n\\r\\nQuos, aliquam nam velit impedit minus tenetur beatae voluptas facere sint pariatur! Voluptatibus, quisquam, error, est assumenda corporis inventore illo nesciunt iure aut dolor possimus repellat minima veniam alia\"}}', '{\"locale\": {\"en\": \"true\"}}', 'lorem-ipsum', '2019-01-24 10:45:04', '2019-01-24 10:45:04', '2019-01-24 10:45:04', NULL),
(6, 1, 'project', 'publish', '{\"en\": {\"body\": \"<p>Est, maiores, fuga sed nemo qui veritatis ducimus placeat odit quisquam dolorum. Rem, sunt, praesentium veniam maiores quia molestias eos fugit eaque ducimus veritatis provident assumenda. Quia, fuga, voluptates voluptatibus quis enim nam asperiores aliquam dignissimos ullam recusandae debitis iste.</p>\\r\\n<p>Dignissimos, beatae, praesentium illum eos autem perspiciatis? Minus, non, tempore, illo, mollitia exercitationem tempora quas harum odio dolores delectus quidem laudantium adipisci ducimus ullam placeat eaque minima quae iure itaque corporis magni nesciunt eius sed dolor doloremque id quasi nisi.</p>\", \"name\": \"Проект первый\", \"preview\": \"Est, maiores, fuga sed nemo qui veritatis ducimus placeat odit quisquam dolorum. Rem, sunt, praesentium veniam maiores quia molestias eos fugit eaque ducimus veritatis provident\"}}', '{\"locale\": {\"en\": \"true\"}}', 'proekt-pervyy', '2019-01-25 08:58:38', '2019-01-25 08:58:38', '2019-01-25 08:58:38', NULL),
(7, 1, 'project', 'publish', '{\"en\": {\"body\": \"<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Excepturi, dolores, quibusdam architecto voluptatem amet fugiat nesciunt placeat provident cumque accusamus itaque voluptate modi quidem dolore optio velit hic iusto vero praesentium repellat commodi ad id expedita cupiditate repellendus possimus unde?</p>\\r\\n<p>Eius consequatur nihil quibusdam! Laborum, rerum, quis, inventore ipsa autem repellat provident assumenda labore soluta minima alias temporibus facere distinctio quas adipisci nam sunt explicabo officia tenetur at ea quos doloribus dolorum voluptate reprehenderit architecto sint libero illo et hic.</p>\", \"name\": \"Проект второй\", \"preview\": \"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Excepturi, dolores, quibusdam architecto voluptatem amet fugiat nesciunt placeat provident cumque accusamus itaque voluptate modi quidem dolore optio velit hic iusto vero praesentium repellat commodi ad id expedita cupiditate repellendus possimus unde?\"}}', '{\"locale\": {\"en\": \"true\"}}', 'proekt-vtoroy', '2019-01-25 08:59:16', '2019-01-25 08:59:16', '2019-01-25 08:59:16', NULL),
(8, 1, 'page', 'publish', '{\"en\": {\"body\": \"<p>+234 23 9873237<br /><a href=\\\"mailto:#\\\">some.email@somewhere.com</a><br /><br />234 Hidden Pond Road, Ashland City, TN 37015</p>\", \"name\": \"Контакты\", \"copyright\": \"Copyright &copy; 2019, Your name. Designed by <a href=\\\"http://gettemplate.com/\\\" rel=\\\"designer\\\">gettemplate</a>\"}}', '{\"locale\": {\"en\": \"true\"}}', 'footer', '2019-01-28 04:04:05', '2019-01-28 04:04:05', '2019-01-28 04:04:05', NULL),
(9, 1, 'page', 'publish', '{\"en\": {\"title\": \"Progressus - Free business bootstrap template by GetTemplate\"}}', '{\"locale\": {\"en\": \"true\"}}', 'setting', '2019-01-28 04:35:48', '2019-01-28 04:23:47', '2019-01-28 04:35:48', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `permissions` json DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `role_users`
--

CREATE TABLE `role_users` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `settings`
--

CREATE TABLE `settings` (
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` json NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `tagged`
--

CREATE TABLE `tagged` (
  `id` int(10) UNSIGNED NOT NULL,
  `taggable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `taggable_id` int(10) UNSIGNED NOT NULL,
  `tag_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `tags`
--

CREATE TABLE `tags` (
  `id` int(10) UNSIGNED NOT NULL,
  `namespace` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `count` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `terms`
--

CREATE TABLE `terms` (
  `id` int(10) UNSIGNED NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` json NOT NULL,
  `term_group` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `term_relationships`
--

CREATE TABLE `term_relationships` (
  `post_id` int(10) UNSIGNED NOT NULL,
  `term_taxonomy_id` int(10) UNSIGNED NOT NULL,
  `term_order` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `term_taxonomy`
--

CREATE TABLE `term_taxonomy` (
  `id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `taxonomy` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `permissions` json DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `last_login`, `permissions`) VALUES
(1, 'admin', 'admin@admin.com', NULL, '$2y$10$iGxdwdIUaPh3zoUpRjPOKeme0QySxihGQv2TFs8KlVzlG6BnznKIq', 'G7ifsiIZLnbnkmTb1DOQNsXSJBA5IYEqpEKjdYVw8KtdxvnlxGVnAwzlfc6N', '2019-01-24 06:39:55', '2019-03-07 03:36:15', '2019-03-07 03:36:15', '{\"platform.index\": \"1\", \"platform.systems\": \"1\", \"platform.bulldozer\": \"1\", \"platform.systems.menu\": \"1\", \"platform.systems.cache\": \"1\", \"platform.systems.index\": \"1\", \"platform.systems.media\": \"1\", \"platform.systems.roles\": \"1\", \"platform.systems.users\": \"1\", \"platform.posts.type.main\": \"1\", \"platform.posts.type.news\": \"1\", \"platform.systems.backups\": \"1\", \"platform.systems.history\": \"1\", \"platform.posts.type.about\": \"1\", \"platform.systems.category\": \"1\", \"platform.systems.comments\": \"1\", \"platform.posts.type.footer\": \"1\", \"platform.posts.type.contact\": \"1\", \"platform.posts.type.project\": \"1\", \"platform.posts.type.setting\": \"1\", \"platform.systems.attachment\": \"1\", \"platform.systems.announcement\": \"1\", \"platform.posts.type.example-page\": \"0\", \"platform.posts.type.example-post\": \"0\"}'),
(2, 'user', 'user@user.com', NULL, '$2y$10$iGxdwdIUaPh3zoUpRjPOKeme0QySxihGQv2TFs8KlVzlG6BnznKIq', 'J7aUlffXtmOBZVE3HIkvp2jebfZ75yauE1WfjMMkjT4A9BuLrvj64S5gTHoX', '2019-01-24 02:39:55', '2019-03-07 03:59:14', '2019-03-07 03:59:14', '{\"platform.index\": \"1\", \"platform.systems\": \"1\", \"platform.bulldozer\": \"0\", \"platform.systems.menu\": \"1\", \"platform.systems.cache\": \"0\", \"platform.systems.index\": \"1\", \"platform.systems.media\": \"1\", \"platform.systems.roles\": \"0\", \"platform.systems.users\": \"0\", \"platform.posts.type.main\": \"1\", \"platform.posts.type.news\": \"1\", \"platform.systems.backups\": \"0\", \"platform.systems.history\": \"0\", \"platform.posts.type.about\": \"1\", \"platform.systems.category\": \"0\", \"platform.systems.comments\": \"0\", \"platform.posts.type.footer\": \"1\", \"platform.posts.type.contact\": \"1\", \"platform.posts.type.project\": \"1\", \"platform.posts.type.setting\": \"1\", \"platform.systems.attachment\": \"1\", \"platform.systems.announcement\": \"0\", \"platform.posts.type.example-page\": \"0\", \"platform.posts.type.example-post\": \"0\"}');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `announcements`
--
ALTER TABLE `announcements`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `attachmentable`
--
ALTER TABLE `attachmentable`
  ADD PRIMARY KEY (`id`),
  ADD KEY `attachmentable_attachmentable_type_attachmentable_id_index` (`attachmentable_type`,`attachmentable_id`),
  ADD KEY `attachmentable_attachment_id_foreign` (`attachment_id`);

--
-- Индексы таблицы `attachments`
--
ALTER TABLE `attachments`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comments_approved_post_id_index` (`approved`,`post_id`),
  ADD KEY `comments_post_id_index` (`post_id`),
  ADD KEY `comments_parent_id_index` (`parent_id`),
  ADD KEY `comments_user_id_foreign` (`user_id`);

--
-- Индексы таблицы `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notifications_notifiable_type_notifiable_id_index` (`notifiable_type`,`notifiable_id`);

--
-- Индексы таблицы `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Индексы таблицы `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `posts_slug_unique` (`slug`),
  ADD KEY `posts_status_type_index` (`status`,`type`),
  ADD KEY `posts_user_id_foreign` (`user_id`);

--
-- Индексы таблицы `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_slug_unique` (`slug`);

--
-- Индексы таблицы `role_users`
--
ALTER TABLE `role_users`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `role_users_role_id_foreign` (`role_id`);

--
-- Индексы таблицы `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`key`);

--
-- Индексы таблицы `tagged`
--
ALTER TABLE `tagged`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tagged_taggable_type_taggable_id_index` (`taggable_type`,`taggable_id`);

--
-- Индексы таблицы `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `terms`
--
ALTER TABLE `terms`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `terms_slug_unique` (`slug`);

--
-- Индексы таблицы `term_relationships`
--
ALTER TABLE `term_relationships`
  ADD KEY `term_relationships_post_id_term_taxonomy_id_index` (`post_id`,`term_taxonomy_id`),
  ADD KEY `term_relationships_term_taxonomy_id_foreign` (`term_taxonomy_id`);

--
-- Индексы таблицы `term_taxonomy`
--
ALTER TABLE `term_taxonomy`
  ADD PRIMARY KEY (`id`),
  ADD KEY `term_taxonomy_id_taxonomy_index` (`id`,`taxonomy`),
  ADD KEY `term_taxonomy_parent_id_foreign` (`parent_id`),
  ADD KEY `term_taxonomy_term_id_foreign` (`term_id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `announcements`
--
ALTER TABLE `announcements`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `attachmentable`
--
ALTER TABLE `attachmentable`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT для таблицы `attachments`
--
ALTER TABLE `attachments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT для таблицы `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT для таблицы `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT для таблицы `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT для таблицы `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `tagged`
--
ALTER TABLE `tagged`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `terms`
--
ALTER TABLE `terms`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `term_taxonomy`
--
ALTER TABLE `term_taxonomy`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `attachmentable`
--
ALTER TABLE `attachmentable`
  ADD CONSTRAINT `attachmentable_attachment_id_foreign` FOREIGN KEY (`attachment_id`) REFERENCES `attachments` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `comments_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `posts_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `role_users`
--
ALTER TABLE `role_users`
  ADD CONSTRAINT `role_users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `role_users_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `term_relationships`
--
ALTER TABLE `term_relationships`
  ADD CONSTRAINT `term_relationships_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `term_relationships_term_taxonomy_id_foreign` FOREIGN KEY (`term_taxonomy_id`) REFERENCES `term_taxonomy` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `term_taxonomy`
--
ALTER TABLE `term_taxonomy`
  ADD CONSTRAINT `term_taxonomy_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `term_taxonomy` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `term_taxonomy_term_id_foreign` FOREIGN KEY (`term_id`) REFERENCES `terms` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
