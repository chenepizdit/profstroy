<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'AppController@index')->name('main');
Route::get('/news', 'AppController@news')->name('news');
Route::get('/about', 'AppController@about')->name('about');
Route::get('/contact', 'AppController@contact')->name('contact');
Route::post('/send', 'AppController@send')->name('send');
Route::get('/news/{slug}', 'AppController@one_news')->name('news');

Route::get('/project', 'AppController@project')->name('project');
Route::get('/project/{slug}', 'AppController@one_project')->name('project');

