<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport"    content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
    <meta name="author"      content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
	
	<title>{{ $setting->content['en']['title'] }}</title>

	<link rel="shortcut icon" href="/favicon.ico">
	
	<link rel="stylesheet" media="screen" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
	<link rel="stylesheet" href="/assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="/assets/css/font-awesome.min.css">

	<!-- Custom styles for our template -->
	<link rel="stylesheet" href="/assets/css/bootstrap-theme.css" media="screen" >
	<link rel="stylesheet" href="/assets/css/main.css">

</head>
<body class="home">
	<!-- Fixed navbar -->
	<div class="navbar navbar-inverse navbar-fixed-top headroom" >
		<div class="container">
			<div class="navbar-header">
				<!-- Button for smallest screens -->
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"><span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
				<a class="navbar-brand" href="index.html"><img src="{{ $logo }}" alt="Logo"></a>
			</div>
			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav pull-right">
                    @foreach ($menu as $item)
                        @if(empty($item->children()->first()))
                            <li class="{{ (Route::currentRouteName() == $item->title) ? 'active' : ''}}"><a href="{{ $item->slug }}">{{ $item->label }}</a></li>
                        @else
                            <li class="dropdown {{ (Route::currentRouteName() == $item->title) ? 'active' : ''}}">
                                <a href="{{ $item->slug }}">{{ $item->label }}<b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    @foreach ($item->children()->get() as $child)
                                        <li><a href="{{ $child->slug }}">{{ $child->label }}</a></li>
                                    @endforeach
                                </ul>
                            </li>
                        @endif
                    @endforeach
				</ul>
			</div><!--/.nav-collapse -->
		</div>
	</div> 
	<!-- /.navbar -->

    @yield('content')

	<footer id="footer" class="top-space">

		<div class="footer1">
			<div class="container">
				<div class="row">
					
					<div class="col-md-3 widget">
						<h3 class="widget-title">{{ $footer->content['en']['name'] }}</h3>
						<div class="widget-body">
							{!! $footer->content['en']['body'] !!}
						</div>
					</div>

				</div> <!-- /row of widgets -->
			</div>
		</div>

		<div class="footer2">
			<div class="container">
				<div class="row">
					
					<div class="col-md-6 widget">
						<div class="widget-body">
							<p class="simplenav">
                                @foreach ($menu as $item)
								    <a href="{{ $item->slug }}">{{ $item->label }}</a>
                                    @if(!$loop->last) 
                                        |
                                    @endif 
                                @endforeach
							</p>
						</div>
					</div>

					<div class="col-md-6 widget">
						<div class="widget-body">
							<p class="text-right">
								{!! $footer->content['en']['copyright'] !!}
							</p>
						</div>
					</div>

				</div> <!-- /row of widgets -->
			</div>
		</div>

	</footer>	

	<!-- JavaScript libs are placed at the end of the document so the pages load faster -->
	<script src="/js/jquery.min.js"></script>
	<script src="/js/bootstrap.min.js"></script>
	<script src="/assets/js/headroom.min.js"></script>
	<script src="/assets/js/jQuery.headroom.min.js"></script>
	<script src="/assets/js/template.js"></script>
</body>
</html>