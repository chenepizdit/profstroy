@component($typeForm,get_defined_vars())
<div    id="cst-upload-image"
        data-fields--upload-storage="{{$storage ?? 'public'}}"
        data-fields--upload-name="{{$name}}"
        data-fields--upload-id="dropzone-{{$id}}"
        data-fields--upload-data="{!!htmlspecialchars(json_encode($value), ENT_QUOTES, 'UTF-8')!!}"
>
    <div class="custom-file-container" data-upload-id="myUploader">

    <label>Загрузить файл <a href="javascript:void(0)" class="custom-file-container__image-clear" title="Clear Image" style="font-weight: 900;">Х</a></label>

    <label class="custom-file-container__custom-file" >
        <input type="hidden" name="MAX_FILE_SIZE" value="10485760" />
        <input type="file" class="custom-file-container__custom-file__custom-file-input" accept="*">
        <span class="custom-file-container__custom-file__custom-file-control"></span>
    </label>

    <div class="custom-file-container__image-preview" style="overflow: hidden;"></div>

    @csrf
    <input type="hidden" class="cst-input-image" name="{{$name}}">
    </div>
</div>
<script>
    $( document ).ready(function() {
        var upload = new FileUploadWithPreview('myUploader');
        var value = $('#cst-upload-image').data('fields--upload-data');
        
        if (value.length) {
            let image = value[0];
            let url = '/storage/' + image.path + '/' + image.name + '.' + image.extension;

            $('.custom-file-container__image-preview')
                .css('background-image', 'url(' + url + ')');

            $('.cst-input-image').val(image.id)
        }

        window.addEventListener('fileUploadWithPreview:imageSelected', function(e) {

            const formData = new FormData();

            formData.append('file', $('.custom-file-container__custom-file__custom-file-input')[0].files[0]);
            formData.append('storage', 'public');
            formData.append('_token', $('[name="_token"]')[0].value);

            $.ajax({
                type: "POST",
                url: "/dashboard/systems/files",
                data: formData,
                processData: false,
                contentType: false,
                dataType: "json",
                success: function(msg) {
                    $('.cst-input-image').val(msg.id)
                }
            });
        });

        let clearButton = document.querySelector('.custom-file-container__image-clear');
        clearButton.addEventListener('click', function () {
            $('.cst-input-image').val('')
        }, true);

    });

</script>
@endcomponent