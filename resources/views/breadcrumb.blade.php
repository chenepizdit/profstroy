<ol class="breadcrumb">
    @foreach ($crumbs as $crumb)
        @if(!$loop->last)
            <li><a href="{{ $crumb->slug }}">{{ $crumb->label }}</a></li>
        @else
            <li class="active">{{ $crumb->label }}</li>
        @endif
    @endforeach
</ol>