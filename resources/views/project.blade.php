@extends('layouts.app')

@section('content')

    <header id="head" class="secondary"></header>

    <!-- container -->
    <div class="container">
        
        @include('breadcrumb', ['crumbs' => $crumbs])

        <div class="row">

            <!-- Article main content -->
            <article class="col-md-12 maincontent">
                @foreach ($content as $item)
                    <header class="page-header">
                        <h1 class="page-title">{{ $item->content['en']['name'] }}</h1>
                    </header>
                    {{ $item->content['en']['preview'] }}
                    <br><br>
                    <a href="/project/{{ $item->slug }}">Подробнее</a>
                @endforeach
            </article>
            <!-- /Article -->

        </div>
    </div>	<!-- /container -->

@endsection