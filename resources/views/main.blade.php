@extends('layouts.app')

@section('content')

	<!-- Header -->
	<header id="head" style="background: #181015 url({{ $imagePath }}) no-repeat;  background-size: cover;">
		<div class="container">
			<div class="row">
				<h1 class="lead">{{ $main->content['en']['name'] }}</h1>
				<p class="tagline">{{ $main->content['en']['description'] }}</p>
			</div>
		</div>
	</header>
	<!-- /Header -->

    <!-- Intro -->
	<div class="container text-center">
		<br> <br>
		<h2 class="thin">{{ $main->content['en']['heading'] }}</h2>
		<span class="text-muted"> {!! $main->content['en']['body'] !!} </span>
	</div>
	<!-- /Intro-->

@endsection