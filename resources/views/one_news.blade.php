@extends('layouts.app')

@section('content')

	<header id="head" class="secondary"></header>

    <!-- container -->
    <div class="container">

        @include('breadcrumb', ['crumbs' => $crumbs])

        <div class="row">
            
            <article class="col-sm-12 maincontent">
                <header class="page-header">
                    <h1 class="page-title">{{ $content->content['en']['name'] }}</h1>
                </header>
                {!! $content->content['en']['body'] !!}
                
            </article>

        </div>
    </div>	<!-- /container -->

@endsection