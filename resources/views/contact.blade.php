@extends('layouts.app')

@section('content')

	<header id="head" class="secondary"></header>

    <!-- container -->
    <div class="container">

        @include('breadcrumb', ['crumbs' => $crumbs])

        <div class="row">
            
            <!-- Article main content -->
            <article class="col-sm-9 maincontent">
                <header class="page-header">
                    <h1 class="page-title">{{ $content->content['en']['name'] }}</h1>
                </header>
                
                {!! $content->content['en']['body'] !!}

                <br>
                    <form action="#" id="form-message">
                        <div class="row">
                            <div class="col-sm-4">
                                <input class="form-control" type="text" placeholder="Имя" name="contact-name" id="input_name" required>
                            </div>
                            <div class="col-sm-4">
                                <input class="form-control" type="email" placeholder="E-mail" name="contact-email"  id="input_email" required>
                            </div>
                            <div class="col-sm-4">
                                <input class="form-control" type="text" placeholder="Телефон" name="contact-phone"  id="input_phone">
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-sm-12">
                                <textarea placeholder="Комментарий" name="textarea-message" id="textarea_message" class="form-control" rows="9" required></textarea>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-sm-6">
                            </div>
                            <div class="col-sm-6 text-right">
                                <input id="btn_submit" name="submit" class="btn btn-action" type="submit" value="отправить">
                            </div>
                        </div>
                    </form>

            </article>
            <!-- /Article -->
            
            <!-- Sidebar -->
            <aside class="col-sm-3 sidebar sidebar-right">

                <div class="widget">
                    <h4>Адрес:</h4>
                    <address>
                        {{ $content->content['en']['address'] }}
                    </address>
                    <h4>Телефон:</h4>
                    <address>
                        {{ $content->content['en']['phone'] }}
                    </address>
                </div>

            </aside>
            <!-- /Sidebar -->

        </div>
    </div>	<!-- /container -->
    <script src="/js/jquery-1.8.3.js"></script>
    <script>
        $( document ).ready(function() {

            $.ajaxSetup({
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $( "#btn_submit" ).on( "click", function(event) {
                if ($("#form-message")[0].checkValidity()) {
                    event.preventDefault();
                    var mydata = $("form").serialize();
                    $.ajax({
                        type: "POST",
                        url: "/send",
                        data: mydata,
                        success: function(data) {
                            $("#input_name").val("");
                            $("#input_email").val("");
                            $("#textarea_message").val("");
                            $("#input_phone").val("");
                            alert('E-mail успешно отправлен!');	
                        },
                        error: function(xhr, textStatus, errorThrown) {
                            try {
                                if (xhr.status === 422) {
                                    let responseText = JSON.parse(xhr.responseText);
                                    let message = '';						
                                    for (let fields in responseText.errors) {
                                        for (let field of responseText.errors[fields]) {
                                            message += field + '\n\r';
                                        }
                                    }
                                    alert(message);
                                } else {
                                    alert(textStatus);
                                }
                            } catch (err) {
                                alert('Ошибка на стороне сервера');
                            }
                        }
                    });
                }
                return true;
            });

        });
    </script>

@endsection